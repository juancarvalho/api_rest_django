# Django Rest + Nginx + PostgreSql [Docker][Basic Example User App]

```console
$ docker build -t django_rest:v1 .
$ python pycompose up -d dev
$ curl -H 'Accept: application/json; indent=4' http://localhost
{
    "users": "http://localhost/users/",
    "groups": "http://localhost/groups/"
}
```