#!/usr/bin/env python
# coding: utf-8

import unittest
from urllib.parse import urljoin
import requests


class TestHome(unittest.TestCase):

    access_token = 'None'

    def test_status(self):
        url = urljoin("http://localhost", "/")
        data = {'nome': 'juancarvalho'}
        response = requests.get(url, json=data)
        print(response.json())
        self.assertEqual(200, response.status_code)


if __name__ == '__main__':
    unittest.main()
